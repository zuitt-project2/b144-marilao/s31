// Controllers contain the functions and business logic of our express js app
// Means all the operations can do will be places in this file
const Task = require('../models/task')


// Controller function for getting all the tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
}

// Controller function for creating a task
module.exports.createTask = (requestBody) => {
	// Create a task object based on the Mongoose model "Task"
	let newTask = new Task({
		name: requestBody.name
	})

	// SAVE
	// The 'then' method will accept the 2 parameters
		// the first parameter will store the result returned by the mongoose "save" method
		// the second parameter will store the 'error' object
	return newTask.save().then((task, error)=> {
		if(error){
			console.log(error)
			// if an error is encountered, the 'return' statement will prevent other line or code below and within the same code block from executing.
			// SInce the following return statement is nested within the "then" method chained to the "save" method, they do not prevent each other from executing code
			// the else statement will no longer be evaluated
			return false;
		}else {
			// if save is successful return the new task object
			return task;
		}
	})
}

// Deleting a task
/*
Business Logic
1. Look or find for the task with the corresponding id provided in the URL/route
2. Delete the task
*/

module.exports.deleteTask = (taskId) => {
	// the "findbyIdAndRemove" Mongoose method will look for a task with the same id provided from the URL and remove/delete the document from MongoDB it looks for the document using the "_id" field
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		}else{
			return removedTask;
		}
	})
}

// Updating a task
/*
Business Logic
1. Get the task with the ID
2. Replace the task's name returned from the database with the "name " property from the request body
3. Save the task

*/
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) =>{
		// If an error is encountered, return a false
		if(error){
			console.log(error);
			return false;
		}
		// Results of the "findById" method will be stored in the "result" parameter
		// It's name property will be reassigned the value of the "name" received from the request
		result.name = newContent.name;

		return result.save().then((updatedTask, error) => {
			if(error){
				console.log(error);
				return false
			}else{
				return updatedTask;
			}
		})
	})
}

// 2. Create a controller function for retrieving a specific task.
// 3. Return the result back to the client/Postman.
module.exports.getTask = (taskId) => {
	// the "findbyIdAndRemove" Mongoose method will look for a task with the same id provided from the URL and remove/delete the document from MongoDB it looks for the document using the "_id" field
	return Task.findById(taskId).then((retrieveTask, err) => {
		if(err){
			console.log(err);
			return false;
		}else{
			return retrieveTask;
		}
	})
}

// 6. Create a controller function for changing the status of a task to "complete".
// 7. Return the result back to the client/Postman.

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) =>{
		// If an error is encountered, return a false
		if(error){
			console.log(error);
			return false;
		}
		// Results of the "findById" method will be stored in the "result" parameter
		// It's name property will be reassigned the value of the "name" received from the request
		result.status = newContent.status;

		return result.save().then((updatedTask, error) => {
			if(error){
				console.log(error);
				return false
			}else{
				return updatedTask;
			}
		})
	})
}
